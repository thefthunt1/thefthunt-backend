<?php

namespace App\QueryBuilder\Filters\Item;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FilterSearchTerm implements Filter
{

    public function __invoke(Builder $query, $value, string $property)
    {
        return $this->addQuery($query, $value);
    }

    private function addQuery(Builder $query, $value)
    {
        return $query
            ->orWhere('id', 'LIKE', '%' . $value . '%')
            ->orWhere('name', 'LIKE', '%' . $value . '%')
            ->orWhere('color', 'LIKE', '%' . $value . '%')
            ->orWhere('brand', 'LIKE', '%' . $value . '%')
            ->orWhere('status', 'LIKE', '%' . $value . '%');
    }
}
