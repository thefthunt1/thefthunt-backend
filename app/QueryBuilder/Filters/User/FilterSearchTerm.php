<?php

namespace App\QueryBuilder\Filters\User;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FilterSearchTerm implements Filter
{

    public function __invoke(Builder $query, $value, string $property)
    {
        return $this->addQuery($query, $value);
    }

    private function addQuery(Builder $query, $value)
    {
        return $query
            ->orWhere('id', 'LIKE', '%' . $value . '%')
            ->orWhere('name', 'LIKE', '%' . $value . '%')
            ->orWhere('email', 'LIKE', '%' . $value . '%')
            ->orWhere('address', 'LIKE', '%' . $value . '%')
            ->orWhere('postcode', 'LIKE', '%' . $value . '%')
            ->orWhere('city', 'LIKE', '%' . $value . '%');
    }
}
