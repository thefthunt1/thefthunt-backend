<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed $id
 * @property mixed $name
 * @property mixed $status
 * @property mixed $description
 * @property mixed $category_id
 * @property mixed $brand
 * @property mixed $color
 * @property mixed $type
 * @property mixed $price
 * @property mixed $category
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property mixed $images
 */
class Item extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i',
        'updated_at' => 'datetime:Y-m-d H:i',
    ];

    public function type(): BelongsTo
    {
        return $this->belongsTo(ItemType::class, 'item_type_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,);
    }

    public function images(): HasMany
    {
        return $this->hasMany(ItemImages::class);
    }

    public function usersBookmarked(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_bookmarks');
    }

    public function scopeCreatedBetween(Builder $query, $start, $end): Builder
    {
        return $query->whereBetween("{$this->table}.created_at", [$start . ' 00:00:00', $end . ' 23:59:59']);
    }
}
