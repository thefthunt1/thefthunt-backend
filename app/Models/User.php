<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property mixed $id
 * @property mixed $name
 * @property mixed $email
 * @property mixed $address
 * @property mixed $postcode
 * @property mixed $city
 * @property mixed $profile_image
 * @property mixed $country
 * @property mixed $created_at
 * @property mixed $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, Billable;

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function scopeCreatedBetween(Builder $query, $start, $end): Builder
    {
        return $query->whereBetween("{$this->table}.created_at", [$start . ' 00:00:00', $end . ' 23:59:59']);
    }

    public function items(): HasMany
    {
        return $this->hasMany(Item::class);
    }

    public function bookmarkList(): BelongsToMany
    {
        return $this->belongsToMany(Item::class, 'user_bookmarks')->withTimestamps();
    }

    public function providers(): HasMany
    {
        return $this->hasMany(AuthProvider::class);
    }
}
