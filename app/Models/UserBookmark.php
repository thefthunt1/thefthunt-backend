<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

//** USE IN CASE IT'S NECESSARY */
class UserBookmark extends Pivot
{
    use HasFactory;

    protected $guarded = ['id'];
}
