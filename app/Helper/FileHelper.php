<?php

namespace App\Helper;

use Illuminate\Support\Facades\Storage;

/**
 * By default, .env variable FILESYSTEM_DISK will be used
 */
class FileHelper
{

    public static function upload(string $path, $image, string $disk = '', $options = 'public')
    {
        if ($disk) {
            return Storage::disk($disk)->put($path, $image, $options);
        }
        return Storage::put($path, $image, $options);
    }

    public static function getUrl(?string $path, string $disk = '')
    {
        if (!$path) return null;
        if(filter_var($path, FILTER_VALIDATE_URL)) return $path;

        if ($disk) {
            return Storage::disk($disk)->url($path);
        }
        return Storage::url($path);
    }

    public static function getFile(string $path, string $disk = '')
    {
        if ($disk) {
            return Storage::disk($disk)->get($path);
        }
        return Storage::get($path);
    }

    public static function delete(string $path, string $disk = '')
    {
        if (!$path) return true;
        if(filter_var($path, FILTER_VALIDATE_URL)) return true;

        if ($disk) {
            return Storage::disk($disk)->delete($path);
        }
        return Storage::delete($path);
    }

    public static function makePublic(string $path, string $disk = '')
    {
        if ($disk) {
            return Storage::disk($disk)->setVisibility($path, 'public');
        }
        return Storage::setVisibility($path, 'public');
    }
}
