<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\BaseController;
use App\Http\Resources\Shared\Country\CountryResource;
use App\Repositories\CountryRepositoryInterface;

class CountryController extends BaseController
{
    public function __construct(private CountryRepositoryInterface $countryRepository)
    {
    }

    public function index()
    {
        return $this->responseSuccess(
            CountryResource::collection($this->countryRepository->getAll()),
            'List of countries'
        );
    }
}
