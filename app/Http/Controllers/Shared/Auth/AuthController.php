<?php

namespace App\Http\Controllers\Shared\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Shared\Auth\LoginRequest;
use App\Http\Requests\Shared\Auth\RegistrationRequest;
use App\Http\Resources\Shared\User\AuthUserResource;
use App\Http\Resources\Shared\User\Model\AuthUser;
use App\Mail\RegistrationEmail;
use App\Models\User;
use App\Repositories\BaseRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends BaseController
{

    public function __construct(private BaseRepositoryInterface $baseRepository)
    {
    }

    public function login(LoginRequest $request): JsonResponse
    {
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return $this->responseError([], 'The provided credentials are incorrect.', 401);
        }
        //Attempt creates session if credentials are matching
        /** @var User $user */
        $user = Auth::user();

        $token = $user->createToken($request->device)->plainTextToken;

        return $this->responseSuccess(new AuthUserResource(new AuthUser($token, $user)), 'Logged in successfully.');
    }

    public function register(RegistrationRequest $request): JsonResponse
    {
        $data = $request->safe()->except('password');
        $data['password'] = Hash::make($request->password);
        $data['profile_image'] = 'https://ui-avatars.com/api/?size=160&name=' . Str::slug($request->name, '+');

        $user = $this->baseRepository->store(User::class, $data);

        $token = $user->createToken($request->device)->plainTextToken;

        Mail::to($user->email)->locale(app()->getLocale())->send(new RegistrationEmail($user));

        return $this->responseSuccess(new AuthUserResource(new AuthUser($token, $user)), 'Registered successfully.');
    }

    public function me(Request $request): JsonResponse
    {
        return $this->responseSuccess(
            new AuthUserResource(new AuthUser($request->bearerToken(), $request->user())),
            'User info.'
        );
    }

    public function logout(): JsonResponse
    {
        auth()->user()->tokens()->delete();

        return $this->responseSuccess([], 'Successfully logged out.');
    }

    public function redirectToProvider(string $provider)
    {
        $validated = $this->validateProvider($provider);
        if (!is_null($validated)) {
            return $validated;
        }

        return $this->responseSuccess(
            [
                'redirect_url' => Socialite::driver($provider)->stateless()->redirect()->getTargetUrl()
            ]
        );
    }

    public function handleProviderCallback(Request $request, string $provider)
    {
        $validated = $this->validateProvider($provider);
        if (!is_null($validated)) {
            return $validated;
        }

        try {
            $user = Socialite::with($provider)->stateless()->userFromToken($request->provider_token);

//            $user = Socialite::driver($provider)->stateless()->user();

            $userCreated = User::firstOrCreate(
                [
                    'email' => $user->getEmail()
                ],
                [
                    'name' => $user->getName(),
                    'email_verified_at' => now(),
                    'profile_image' => 'https://ui-avatars.com/api/?size=160&name=' . Str::slug($user->name, '+'),
//                    'profile_image' => $user->avatar ?? 'https://ui-avatars.com/api/?size=160&name=' . Str::slug($user->name, '+'),
                ]
            );

            $userCreated->providers()->updateOrCreate(
                [
                    'auth_provider' => $provider,
                    'auth_provider_id' => $user->getId(),
                ]
            );

            $token = $userCreated->createToken('Social Login')->plainTextToken;

            return $this->responseSuccess(
                new AuthUserResource(new AuthUser($token, $userCreated)),
                'Signed in successfully.'
            );
        } catch (\Exception $e) {
            return $this->responseError([], $e->getMessage());
        }
    }

    /**
     * @param $provider
     * @return JsonResponse
     */
    protected function validateProvider($provider)
    {
        if (!in_array($provider, ['facebook', 'github', 'google'])) {
            return response()->json(['error' => 'Please login using facebook, github or google'], 422);
        }
    }
}
