<?php

namespace App\Http\Controllers\Admin;

use App\Helper\FileHelper;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Admin\User\CreateAndUpdateUserRequest;
use App\Http\Resources\Admin\User\CreateUserItem;
use App\Http\Resources\Admin\User\EditUserItem;
use App\Http\Resources\Admin\User\Model\CreateUser;
use App\Http\Resources\Admin\User\Model\EditUser;
use App\Http\Resources\Admin\User\Model\ShowUser;
use App\Http\Resources\Admin\User\ShowUserItem;
use App\Http\Resources\Admin\User\UserResource;
use App\Models\User;
use App\Repositories\CountryRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Services\UserService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends BaseController
{

    public function __construct(
        private UserRepositoryInterface $userRepository,
        private CountryRepositoryInterface $countryRepository,
        private UserService $userService,
    )
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->responseSuccessWithPagination(
            UserResource::collection($this->userRepository->getAll()),
            'List of users'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        return $this->responseSuccess(
            new CreateUserItem(
                new CreateUser(
                    countries: $this->countryRepository->getAll()
                )
            ),
            'List of items for create new User.'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateAndUpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateAndUpdateUserRequest $request)
    {
        try {
            $data = $request->safe()->except('profile_image');
            $data['password'] = Hash::make($request->password);

            $user = $this->userRepository->store(User::class, $data);

            if ($request->has('profile_image')) {
                $imagePath = $this->userService->uploadAndGetImagePath($user, $request->profile_image);
            }else{
                $imagePath = 'https://ui-avatars.com/api/?size=160&name=' . Str::slug($request->name, '+');
            }

            $this->userRepository->update($user, ['profile_image' => $imagePath]);

            return $this->responseSuccess(['user_id' => $user->id], 'User successfully created.');
        } catch (\Exception $exception) {
            return $this->responseError([], $exception->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $user = $this->userRepository->findWithoutGlobalScopes(User::class, $id);
        if (!$user) {
            return $this->notFoundError();
        }

        return $this->responseSuccess(
            new ShowUserItem(
                new ShowUser(
                    user:  $user,
                    items: $user->items
                )
            ),
            'User data'
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(int $id)
    {
        $user = $this->userRepository->findWithoutGlobalScopes(User::class, $id);
        if (!$user) {
            return $this->notFoundError();
        }

        return $this->responseSuccess(
            new EditUserItem(
                new EditUser(
                    user:      $user,
                    countries: $this->countryRepository->getAll()
                )
            ),
            'User data'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateAndUpdateUserRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateAndUpdateUserRequest $request, int $id)
    {
        $user = $this->userRepository->findWithoutGlobalScopes(User::class, $id);
        if (!$user) {
            return $this->notFoundError();
        }

        try {
            $validatedData = $request->safe()->except(['profile_image', 'method']);
            if ($request->has('profile_image')) {
                FileHelper::delete($user->profile_image);
                $imagePath = $this->userService->uploadAndGetImagePath($user, $request->profile_image);
                $validatedData['profile_image'] = $imagePath;
            }

            $this->userRepository->update($user, $validatedData);

            return $this->responseSuccess([], 'User successfully updated.');
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $user = $this->userRepository->findWithoutGlobalScopes(User::class, $id);
        if (!$user) {
            return $this->notFoundError();
        }

        try {
            FileHelper::delete($user->profile_image);
            $user->delete();

            return $this->responseSuccess([], 'User successfully deleted.');
        }catch (\Exception $e){
            return $this->responseError([], $e->getMessage(), 500);
        }
    }
}
