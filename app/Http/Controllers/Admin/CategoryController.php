<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Admin\Category\CreateOrUpdateCategoryRequest;
use App\Http\Resources\Admin\Category\CategoryItem;
use App\Http\Resources\Admin\Category\CategoryResource;
use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\ImageService;

class CategoryController extends BaseController
{

    public function __construct(
        private CategoryRepositoryInterface $categoryRepository,
        private ImageService $imageService
    )
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCategories = $this->categoryRepository->getAll();

        return $this->responseSuccessWithPagination(
            CategoryResource::collection($allCategories),
            'Category tree'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrUpdateCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateOrUpdateCategoryRequest $request)
    {
        try {
            $category = Category::create($request->validated());

            return $this->responseSuccess(['category_id' => $category->id], 'Successfully created category');
        }catch (\Exception $exception){
            return $this->responseError([], $exception->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(int $id)
    {
        $category = $this->categoryRepository->findWithoutGlobalScopes(Category::class, $id);
        if (!$category) {
            return $this->notFoundError();
        }

        return $this->responseSuccess(
            new CategoryItem($category),
            'Category data'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateOrUpdateCategoryRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateOrUpdateCategoryRequest $request, $id)
    {
        $category = $this->categoryRepository->findWithoutGlobalScopes(Category::class, $id);
        if (!$category) {
            return $this->notFoundError();
        }

        try {
            $validatedData = $request->safe()->except(['image']);
            if ($request->has('image')) {
                $imagePath = $this->imageService->uploadAndGetImagePath($request->image, 'categories/');
                $validatedData['image'] = $imagePath;
            }

            $this->categoryRepository->update($category, $validatedData);

            return $this->responseSuccess([], 'Category successfully updated.');
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $category = $this->categoryRepository->findWithoutGlobalScopes(Category::class, $id);
        if (!$category) {
            return $this->notFoundError();
        }

        try {
            $category->delete();

            return $this->responseSuccess([], 'Category successfully deleted.');
        }catch (\Exception $e){
            return $this->responseError([], $e->getMessage(), 500);
        }
    }
}
