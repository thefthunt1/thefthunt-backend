<?php

namespace App\Http\Controllers\Admin;

use App\Helper\FileHelper;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Admin\Item\CreateAndUpdateItemRequest;
use App\Http\Resources\Admin\Item\CreateItem;
use App\Http\Resources\Admin\Item\EditItem;
use App\Http\Resources\Admin\Item\ItemResource;
use App\Http\Resources\Admin\Item\Model\CreateItemModel;
use App\Http\Resources\Admin\Item\Model\EditItemModel;
use App\Models\Item;
use App\Models\ItemImages;
use App\Models\ItemType;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;
use App\Repositories\ItemRepositoryInterface;
use App\Services\ItemService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ItemController extends BaseController
{

    public function __construct(
        private ItemRepositoryInterface $itemRepository,
        private CategoryRepositoryInterface $categoryRepository,
        private CountryRepositoryInterface $countryRepository,
        private ItemService $itemService
    )
    {
    }
    /**
     * @return JsonResponse
     */
    public function getItemTypes()
    {
        return $this->responseSuccess(ItemType::all()->toArray(), 'Success');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->responseSuccessWithPagination(
            ItemResource::collection($this->itemRepository->getAll()),
            'List of items'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        return $this->responseSuccess(
            new CreateItem(
                new CreateItemModel(
                    categories: $this->categoryRepository->getAll(),
                    types: ItemType::all(),
                    countries: $this->countryRepository->getAll()
                )
            ),
            'Item data'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateAndUpdateItemRequest $request
     * @return JsonResponse
     */
    public function store(CreateAndUpdateItemRequest $request)
    {
        try {
            $item = $this->itemRepository->store(Item::class, $request->safe()->except('images'));
            $this->itemService->uploadAndSaveImages($item, $request->images);

            return $this->responseSuccess(['item' => $item->id], 'Item successfully created.');
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        $item = $this->itemRepository->findWithoutGlobalScopes(Item::class, $id);
        if (!$item) {
            return $this->notFoundError();
        }

        return $this->responseSuccess(
            new EditItem(
                new EditItemModel(
                    item:      $item,
                    categories: $this->categoryRepository->getAll(),
                    types: ItemType::all(),
                    countries: $this->countryRepository->getAll()
                )
            ),
            'Item data'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateAndUpdateItemRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(CreateAndUpdateItemRequest $request, int $id)
    {
        $item = $this->itemRepository->findWithoutGlobalScopes(Item::class, $id);
        if (!$item) {
            return $this->notFoundError();
        }

        try {
            //check if even there are images
            $data = $request->safe()->except('images');
            if ($data) {
                $this->itemRepository->update($item, $data);
            }

            if ($request->has('images')) {
                $this->itemService->uploadAndSaveImages($item, $request->images);
            }

            return $this->responseSuccess([], 'Item successfully updated.');
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        $item = $this->itemRepository->findWithoutGlobalScopes(Item::class, $id);
        if (!$item) {
            return $this->notFoundError();
        }

        try {
            $item->delete();

            return $this->responseSuccess([], 'Item successfully deleted.');
        }catch (\Exception $e){
            return $this->responseError([], $e->getMessage(), 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroyImage(int $id)
    {
        $itemImage = $this->itemRepository->findWithoutGlobalScopes(ItemImages::class, $id);
        if (!$itemImage) {
            return $this->notFoundError();
        }

        try {
            FileHelper::delete($itemImage->image_path);
            $itemImage->forceDelete();

            return $this->responseSuccess([], 'Successfully deleted!');
        } catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }
}
