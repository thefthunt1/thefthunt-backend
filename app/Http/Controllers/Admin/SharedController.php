<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;

class SharedController extends BaseController
{
    public function getInitialData()
    {
        $newItemsCount = 5;

        return $this->responseSuccess(['new_items_badge' => $newItemsCount],'List of data');
    }
}
