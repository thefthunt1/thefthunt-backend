<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    /**
     * @param $data
     * @param string $message
     * @param int $code
     * @param array $additional
     * @return JsonResponse
     */
    public function responseSuccess($data, string $message = 'Success', array $additional = [], int $code = 200): JsonResponse
    {
        $responseData = $this->formatResponseData($data, true, $message, $additional);

        return response()->json($responseData, $code);
    }

    /**
     * @param $data
     * @param $message
     * @param int $code
     * @param array $additional
     * @return JsonResponse
     */
    public function responseError($data, $message, int $code = 404, array $additional = []): JsonResponse
    {
        $responseData = $this->formatResponseData($data, false, $message, $additional);

        return response()->json($responseData, $code);
    }

    /**
     * @param $data
     * @param $success
     * @param $message
     * @param array $additional
     * @return array
     */
    private function formatResponseData($data, $success, $message, $additional = []): array
    {
        $return = [
            'data' => $data,
            'success' => $success,
            'message' => $message
        ];
        if ($additional) {
            return array_merge($additional, $return);
        }

        return $return;
    }

    protected function responseSuccessWithPagination($data, $message = 'Success', $additional = [])
    {
        $responseData = [
            'message' => $message,
            'success' => true
        ];

        $additional = array_merge($additional, $responseData);

        return $data->additional($additional);
    }

    /**
     * @return JsonResponse
     */
    public function accessForbidden(): JsonResponse
    {
        $response = [
            'success' => false,
            'message' => 'Access Forbidden.',
        ];

        return response()->json($response, 403);
    }

    protected function notFoundError(): JsonResponse
    {
        return $this->responseError(null, 'Not found');
    }
}
