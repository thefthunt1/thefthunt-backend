<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\API\Item\ItemResource;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function bookmarkItem(Request $request): JsonResponse
    {
        $user = $request->user();
        $bookmarked = $this->userRepository->bookmarkItem($user, $request->item_id);

        return $this->responseSuccess([], $bookmarked ? 'Successfully bookmarked!' : 'Item removed from bookmarks');
    }

    public function bookmarkList(Request $request)
    {
        $user = $request->user();
        $list = $user->bookmarkList()->with('images')->paginate(config('api.pagination.items.list'));

        return $this->responseSuccessWithPagination(ItemResource::collection($list), 'Successfully bookmarked!');
    }
}
