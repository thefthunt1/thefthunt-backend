<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Resources\API\Category\CategoryItem;
use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    public function __construct(
        private CategoryRepositoryInterface $categoryRepository
    )
    {}

    /**
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        $category = $this->categoryRepository->find(Category::class, $id);
        if (!$category) {
            return $this->notFoundError();
        }

        return $this->responseSuccess(
            new CategoryItem($category),
            'Category data'
        );
    }
}
