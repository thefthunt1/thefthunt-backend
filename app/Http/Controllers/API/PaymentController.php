<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PaymentController extends BaseController
{
    /**
     * Currently only for Stripe
     *
     * @param Request $request
     * @param string $gateway
     * @return JsonResponse
     */
    public function generateCheckoutUrl(Request $request, string $gateway): JsonResponse
    {
        $itemId = '&item_id=' . $request->get('item_id');
        $checkout =  $request->user()->checkoutCharge(1200, 'T-Shirt',
            sessionOptions: [
            'success_url' => config('payments.stripe.success_url') . $itemId, // + item_id={$request->get('item')}
            'cancel_url' => config('payments.stripe.success_url') . $itemId,
        ]);


        return $this->responseSuccess(['url' => $checkout->url]);
    }


    public function fetchPayment(Request $request, string $gateway)
    {
        $checkoutSession = $request->user()->stripe()->checkout->sessions->retrieve($request->get('session_id'));
    }
}
