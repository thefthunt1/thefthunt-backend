<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\API\Item\CreateOrUpdateItemRequest;
use App\Http\Resources\Admin\Item\CreateItem;
use App\Http\Resources\Admin\Item\Model\CreateItemModel;
use App\Http\Resources\API\Item\ItemResource;
use App\Http\Resources\API\Item\ShowItem;
use App\Models\Item;
use App\Models\ItemType;
use App\Models\User;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;
use App\Repositories\ItemRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Services\ItemService;
use Illuminate\Http\JsonResponse;

class ItemController extends BaseController
{
    public function __construct(
        private ItemRepositoryInterface $itemRepository,
        private UserRepositoryInterface $userRepository,
        private CategoryRepositoryInterface $categoryRepository,
        private CountryRepositoryInterface $countryRepository,
        private ItemService $itemService
    )
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->guard('sanctum')->user();
        $bookmarkList = $user?->bookmarkList()->get();

        $list = $this->itemRepository->getAll();
        if ($bookmarkList) {
            foreach ($list as $item) {
                $item->bookmarked = $bookmarkList->contains('id', $item->id);
            }
        }

        return $this->responseSuccessWithPagination(
            ItemResource::collection($list),
            'List of items'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        return $this->responseSuccess(
            new CreateItem(
                new CreateItemModel(
                    categories: $this->categoryRepository->getAll(),
                    types: ItemType::all(),
                    countries: $this->countryRepository->getAll()
                )
            ),
            'Item data'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrUpdateItemRequest $request
     * @return JsonResponse
     */
    public function store(CreateOrUpdateItemRequest $request)
    {
        try {
            $validatedData = $request->safe()->except('images');
            $validatedData['user_id'] = $request->user()->id;
            $item = $this->itemRepository->store(Item::class, $validatedData);
            $this->itemService->uploadAndSaveImages($item, $request->images);

            return $this->responseSuccess(['item' => $item->id], 'Item successfully created.');
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    public function show(int $id)
    {
        $item = $this->itemRepository->findWithoutGlobalScopes(Item::class, $id);
        if (!$item) {
            return $this->notFoundError();
        }

        /** @var $user User */
        $user = auth()->guard('sanctum')->user();
        if ($user) {
            $item->is_bookmarked = $this->userRepository->hasBookmarkedItem($user, $item->id);
        }
        $item->related_items = $this->itemRepository->getRelatedItems($item, 4);

        return $this->responseSuccess(new ShowItem($item));
    }
}
