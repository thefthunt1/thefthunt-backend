<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\API\Order\CreateOrderRequest;
use App\Models\Item;
use App\Repositories\ItemRepositoryInterface;
use App\Services\ItemService;
use App\Services\Payments\Gateways\StripeGateway;
use App\Services\Payments\PaymentGatewayContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    public function __construct(
        private ItemRepositoryInterface $itemRepository,
        private ItemService $itemService,
        private PaymentGatewayContract $gateway
    )
    {
        $this->gateway = new StripeGateway();
    }

    /**
     * @param CreateOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateOrderRequest $request): JsonResponse
    {
        try {
            $data = $request->safe()->except('images');
            $data['user_id'] = $request->user()->id;
            $data['active'] = false;

            $item = $this->itemRepository->store(Item::class, $data);
            $this->itemService->uploadAndSaveImages($item, $request->images);

            if (!$item) {
                return $this->responseError([], 'Something went wrong, please try again!');
            }

            $response = $this->gateway->initialize($request->user(), [
                'item_id' => $item->id
            ]);

            return response()->json($response->toArray());
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function postback(Request $request): JsonResponse
    {
        $itemId = $request->get('item_id');
        $sessionId = $request->get('session_id');

        if (!$itemId || !$sessionId) {
            return $this->responseError([], 'Missing required fields!');
        }

        $item = $this->itemRepository->findWithoutGlobalScopes(Item::class, $request->get('item_id'));
        if (!$item) {
            return $this->notFoundError();
        }

        try {
            $response = $this->gateway->response($request->user(), ['session_id' => $request->get('session_id')]);
            if ($response->isSuccess()) {
                $this->itemRepository->update($item, ['active' => true]);
            }

            return response()->json($response->toArray());
        }catch (\Exception $exception){
            return $this->responseError(['trace' => $exception->getTraceAsString()], $exception->getMessage(), 400);
        }
    }
}
