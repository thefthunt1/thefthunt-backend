<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\API\Comment\CreateOrUpdateRequest;
use App\Http\Resources\API\Comment\CommentResource;
use App\Models\Comment;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Http\JsonResponse;

class CommentController extends BaseController
{
    public function __construct(
        private CommentRepositoryInterface $commentRepository
    )
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->responseSuccessWithPagination(CommentResource::collection($this->commentRepository->getAll()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrUpdateRequest $request
     * @return JsonResponse
     */
    public function store(CreateOrUpdateRequest $request)
    {
        $user = $request->user();

        $data = $request->validated();
        $data['user_id'] = $user->id;

        $this->commentRepository->store(Comment::class, $data);

        return $this->responseSuccess([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateOrUpdateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(CreateOrUpdateRequest $request, int $id)
    {
        $user = request()->user();
        $comment = $this->commentRepository->findWithoutGlobalScopes(Comment::class, $id);
        if (!$comment) {
            return $this->notFoundError();
        }
        if ($comment->user_id !== $user->id) {
            return $this->responseError([], 'Not allowed to edit this message!', 403);
        }

        try {
            $this->commentRepository->update($comment, $request->validated());

            return $this->responseSuccess([], 'Comment successfully updated.');
        }catch (\Exception $e) {
            return $this->responseError([], $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        $user = request()->user();
        $comment = $this->commentRepository->findWithoutGlobalScopes(Comment::class, $id);
        if (!$comment) {
            return $this->notFoundError();
        }
        if ($comment->user_id !== $user->id) {
            return $this->responseError([], 'Not allowed to delete this message!', 403);
        }

        try {
            $comment->delete();

            return $this->responseSuccess([], 'Comment successfully deleted.');
        }catch (\Exception $e){
            return $this->responseError([], $e->getMessage(), 500);
        }
    }
}
