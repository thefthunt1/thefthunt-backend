<?php

namespace App\Http\Requests\API\Comment;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $required = $this->route()->getName() === 'comments.store' ? 'required|' : '';

        return [
            'item_id' => $required . 'integer',
            'comment' => $required . 'string|max:1000'
        ];
    }
}
