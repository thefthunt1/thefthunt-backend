<?php

namespace App\Http\Requests\API\Item;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $images
 */
class CreateOrUpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $required = $this->route()->getName() === 'items.store' ? 'required|' : '';

        return [
            'item_type_id' => $required . 'integer',
            'category_id' => $required . 'integer',
            'name' => $required . 'string|min:3|max:30',
            'description' => $required . 'string|min:3|max:500',
            'brand' => 'string|min:2|max:50',
            'color' => 'string|max:50',
            'price' => 'numeric|min:1|max:5000',
            'images' => $required . 'array',
            'images.*' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'address' => 'string|min:3|max:40',
            'postcode' => 'string|min:3|max:15',
            'city' => 'string|min:3|max:40',
            'country_id' => $required . 'integer',
        ];
    }
}
