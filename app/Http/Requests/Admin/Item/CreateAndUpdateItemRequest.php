<?php

namespace App\Http\Requests\Admin\Item;

use App\Models\Item;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $images
 */
class CreateAndUpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if ($this->route()->getName() === 'items.store')
        {
            return $this->createRules();
        }

        return $this->updateRules();
    }

    public function createRules(): array
    {
        return [
            'user_id' => 'required|integer',
            'item_type_id' => 'required|integer',
            'category_id' => 'required|integer',
            'name' => 'required|string|min:3|max:30',
            'description' => 'required|string|min:3|max:500',
            'brand' => 'string|min:2|max:50',
            'color' => 'string|max:50',
            'price' => 'numeric|min:1|max:5000',
            'images' => 'required|array',
            'images.*' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }

    public function updateRules(): array
    {
        return [
            'user_id' => 'integer',
            'item_type_id' => 'integer',
            'category_id' => 'integer',
            'name' => 'string',
            'description' => 'string',
            'brand' => 'string',
            'color' => 'string',
            'price' => 'numeric|min:1|max:5000',
            'images' => 'array',
            'images.*' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }
}
