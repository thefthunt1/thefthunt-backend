<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $profile_image
 */
class CreateAndUpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if ($this->route()->getName() === 'users.store')
        {
            return $this->createRules();
        }

        return $this->updateRules();
    }

    public function createRules(): array
    {
        return [
            'name' => 'required|string|min:2|max:40',
            'email' => 'required|unique:users,email|string|min:1|max:40',
            'password' => 'required|string|min:3|max:40',
            'address' => 'string|min:3|max:40',
            'postcode' => 'string|min:3|max:15',
            'city' => 'string|min:3|max:40',
            'country_id' => 'required|integer',
            'profile_image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }

    public function updateRules(): array
    {
        return [
            'name' => 'string|min:2|max:40',
            'email' => 'unique:users,email|string|min:1|max:40',
            'password' => 'string|min:3|max:40',
            'address' => 'string|min:3|max:40',
            'postcode' => 'string|min:3|max:15',
            'city' => 'string|min:3|max:40',
            'country_id' => 'integer',
            'profile_image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }
}
