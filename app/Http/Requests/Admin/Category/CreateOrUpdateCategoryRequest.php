<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $image
 */
class CreateOrUpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $required = $this->route()->getName() === 'categories.store' ? 'required|' : '';

        return [
            'name' => $required . '|string|min:3|max:30',
            'is_active' => 'boolean',
            'parent_id' => 'nullable|integer',
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }
}
