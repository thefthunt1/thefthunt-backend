<?php

namespace App\Http\Requests\Shared\Auth;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $device
 * @property mixed $email
 * @property mixed $password
 */
class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required','string', 'max:40'],
            'email' => ['required', 'unique:users,email', 'email'],
            'password' => ['required', 'min:5'],
            'address' => ['nullable', 'string', 'min:5'],
            'remember_me' => ['nullable', 'sometimes', 'boolean'],
            'device' => ['required']
        ];
    }
}
