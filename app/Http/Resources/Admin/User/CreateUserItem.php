<?php

namespace App\Http\Resources\Admin\User;

use App\Http\Resources\Admin\User\Model\CreateUser;
use App\Http\Resources\Shared\Country\CountryResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CreateUserItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this CreateUser */
        return [
            'countries' => CountryResource::collection($this->getCountries())
        ];
    }
}
