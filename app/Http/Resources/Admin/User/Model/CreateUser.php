<?php

namespace App\Http\Resources\Admin\User\Model;

class CreateUser
{
    /**
     * Create User constructor.
     */
    public function __construct(protected $countries)
    {
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
