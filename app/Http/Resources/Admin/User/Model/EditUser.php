<?php

namespace App\Http\Resources\Admin\User\Model;

class EditUser extends CreateUser
{
    /**
     * Create User constructor.
     */
    public function __construct(
        private $user,
        protected $countries
    )
    {
        parent::__construct($countries);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}
