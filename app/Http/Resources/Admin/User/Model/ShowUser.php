<?php

namespace App\Http\Resources\Admin\User\Model;

class ShowUser
{
    /**
     * Create User constructor.
     */
    public function __construct(protected $user, protected $items)
    {
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }
}
