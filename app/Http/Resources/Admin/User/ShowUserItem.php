<?php

namespace App\Http\Resources\Admin\User;

use App\Http\Resources\Admin\Item\ItemResource;
use App\Http\Resources\Admin\User\Model\EditUser;
use App\Http\Resources\Admin\User\Model\ShowUser;
use App\Http\Resources\Shared\Country\CountryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShowUserItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this ShowUser */
        return [
            'user_info' => new UserResource($this->getUser()),
            'items' => ItemResource::collection($this->getItems())
        ];
    }
}
