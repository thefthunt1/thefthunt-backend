<?php

namespace App\Http\Resources\Admin\User;

use App\Http\Resources\Admin\User\Model\EditUser;
use App\Http\Resources\Shared\Country\CountryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EditUserItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this EditUser */
        return [
            'user' => new UserResource($this->getUser()),
            'countries' => CountryResource::collection($this->getCountries())
        ];
    }
}
