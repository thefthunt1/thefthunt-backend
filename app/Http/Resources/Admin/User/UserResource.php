<?php

namespace App\Http\Resources\Admin\User;

use App\Helper\FileHelper;
use App\Http\Resources\Shared\Country\CountryResource;
use App\Models\ItemImages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this User */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'address' => $this->address,
            'postcode' => $this->postcode,
            'city' => $this->city,
            'profile_image' => FileHelper::getUrl($this->profile_image),
            'country' => new CountryResource($this->country),
            'created_at' => $this->created_at->format('Y-m-d'),
            'updated_at' => $this->updated_at->format('Y-m-d')
        ];
    }
}
