<?php

namespace App\Http\Resources\Admin\Item;

use App\Http\Resources\Admin\Category\CategoryResource;
use App\Http\Resources\Admin\Item\Model\EditItemModel;
use App\Http\Resources\Admin\Item\Type\ItemTypeResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EditItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this EditItemModel */
        return [
            'item' => new SingleItem($this->getItem()),
            'categories' => CategoryResource::collection($this->getCategories()),
            'types' => ItemTypeResource::collection($this->getTypes())
        ];
    }
}
