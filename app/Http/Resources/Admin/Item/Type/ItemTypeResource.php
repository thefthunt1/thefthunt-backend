<?php

namespace App\Http\Resources\Admin\Item\Type;

use App\Http\Resources\Admin\Category\CategoryItem;
use App\Http\Resources\Admin\Category\CategoryResource;
use App\Models\Item;
use App\Models\ItemType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this ItemType */
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
