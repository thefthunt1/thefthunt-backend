<?php

namespace App\Http\Resources\Admin\Item\Image;

use App\Helper\FileHelper;
use App\Models\ItemImages;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this ItemImages */
        return [
            'id' => $this->id,
            'image_path' => FileHelper::getUrl($this->image_path),
        ];
    }
}
