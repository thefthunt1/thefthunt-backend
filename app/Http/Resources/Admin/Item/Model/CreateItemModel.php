<?php

namespace App\Http\Resources\Admin\Item\Model;

class CreateItemModel
{
    /**
     * Create Item constructor.
     */
    public function __construct(
        protected $categories,
        protected $types,
        protected $countries
    ) {
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
