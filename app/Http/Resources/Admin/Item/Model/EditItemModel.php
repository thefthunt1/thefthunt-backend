<?php

namespace App\Http\Resources\Admin\Item\Model;

class EditItemModel extends CreateItemModel
{
    /**
     * Edit Item constructor.
     */
    public function __construct(
        private $item,
        protected $categories,
        protected $types,
        protected $countries
    )
    {
        parent::__construct($categories, $types, $this->countries);
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
