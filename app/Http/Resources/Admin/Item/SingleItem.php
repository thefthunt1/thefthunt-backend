<?php

namespace App\Http\Resources\Admin\Item;

use App\Http\Resources\Admin\Category\CategoryItem;
use App\Http\Resources\Admin\Item\Image\ItemImageResource;
use App\Http\Resources\Admin\Item\Type\ItemTypeResource;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this Item */
        return [
            'id' => $this->id,
            'type' => new ItemTypeResource($this->type),
            'category' => new CategoryItem($this->category),
            'name' => $this->name,
            'status' => $this->status,
            'description' => $this->description,
            'price' => $this->price,
            'color' => $this->color,
            'brand' => $this->brand,
            'images' => ItemImageResource::collection($this->images),
            'created_at' => $this->created_at?->format('Y-m-d H:i'),
            'updated_at' => $this->updated_at?->format('Y-m-d H:i'),
        ];
    }
}
