<?php

namespace App\Http\Resources\Admin\Item;

use App\Http\Resources\Admin\Category\CategoryResource;
use App\Http\Resources\Admin\Item\Model\CreateItemModel;
use App\Http\Resources\Admin\Item\Type\ItemTypeResource;
use App\Http\Resources\Shared\Country\CountryResource;
use App\Models\ItemType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CreateItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this CreateItemModel */
        return [
            'categories' => CategoryResource::collection($this->getCategories()),
            'types' => ItemTypeResource::collection($this->getTypes()),
            'countries' => CountryResource::collection($this->getCountries())
        ];
    }
}
