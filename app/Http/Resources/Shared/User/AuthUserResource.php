<?php

namespace App\Http\Resources\Shared\User;

use App\Helper\FileHelper;
use App\Http\Resources\Shared\User\Model\AuthUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this AuthUser */
        /** @var $user User */

        $user = $this->getUser();

        if ($user->profile_image) {
            $profileImage = FileHelper::getUrl($user->profile_image) ;
        }else{
            $profileImage = FileHelper::getUrl(config('api.users.no-image'));
        }

        return [
            'name' => $user->name,
            'email' => $user->email,
            'token' => $this->getToken(),
            'profile_image' => $profileImage,
        ];
    }
}
