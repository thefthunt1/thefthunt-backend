<?php

namespace App\Http\Resources\Shared\User\Model;

class AuthUser
{
    /**
     * AuthUser constructor.
     *
     * @param $token
     * @param $user
     */
    public function __construct(
        private $token,
        private $user
    )
    {}

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}
