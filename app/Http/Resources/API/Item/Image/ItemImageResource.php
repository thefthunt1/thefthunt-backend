<?php

namespace App\Http\Resources\API\Item\Image;

use App\Helper\FileHelper;
use App\Models\ItemImages;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var $this ItemImages */
        return [
            'id' => $this->id,
            'image_path' => FileHelper::getUrl($this->image_path),
        ];
    }
}
