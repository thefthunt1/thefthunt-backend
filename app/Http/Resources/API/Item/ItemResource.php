<?php

namespace App\Http\Resources\API\Item;

use App\Http\Resources\Admin\Item\Image\ItemImageResource;
use App\Models\Item;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Route;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $bookmarked = $this->bookmarked ?? false;
        if (Route::currentRouteName() === 'bookmarks.list') {
            $bookmarked = true;
        }

        /** @var $this Item */
        return [
            'id' => $this->id,
            'image' => $this->images->count() > 0 ? new ItemImageResource($this->images->first()) : null,
            'name' => $this->name,
            'status' => $this->status,
            'bookmarked' => $bookmarked,
            'price' => $this->price,
            'description' => $this->description
        ];
    }
}
