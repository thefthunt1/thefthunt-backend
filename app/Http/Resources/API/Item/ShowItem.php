<?php

namespace App\Http\Resources\API\Item;

use App\Http\Resources\Admin\Item\Image\ItemImageResource;
use App\Http\Resources\API\Category\CategoryItem;
use App\Models\Item;
use Illuminate\Http\Resources\Json\JsonResource;

class ShowItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var $this Item */
        return [
            'name' => $this->name,
            'reward_price' => $this->price,
            'category' => new CategoryItem($this->category),
            'images' => ItemImageResource::collection($this->images),
            'status' => $this->status,
            'description' => $this->description,
            'color' => $this->color,
            'brand' => $this->brand,
            'created_at' => $this->created_at?->format('Y-m-d H:i'),
            'bookmarked' => $this->is_bookmarked ?? false,
            'related_items' => ItemResource::collection($this->related_items ?? [])
        ];
    }
}
