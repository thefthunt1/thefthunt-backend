<?php

namespace App\Http\Resources\API\Comment;

use App\Helper\FileHelper;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var $this User */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'profile_image' => FileHelper::getUrl($this->profile_image)
        ];
    }
}
