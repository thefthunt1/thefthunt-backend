<?php

namespace App\Http\Resources\API\Comment;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this Comment */
        return [
            'id' => $this->id,
            'user' => new UserItem($this->user),
            'comment' => $this->comment,
            'created_at' => $this->created_at->format('Y-m-d H:i')
        ];
    }
}
