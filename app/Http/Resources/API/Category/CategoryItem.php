<?php

namespace App\Http\Resources\API\Category;

use App\Helper\FileHelper;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this Category */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => FileHelper::getUrl($this->image),
        ];
    }
}
