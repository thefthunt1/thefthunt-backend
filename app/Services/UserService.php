<?php

namespace App\Services;

use App\Helper\FileHelper;
use App\Models\User;
use Illuminate\Support\Str;

class UserService
{

    public function uploadAndGetImagePath(User $user, $image): string
    {
        $basePath = "users/$user->id/profile_image/";
        $imagePath = $basePath . Str::random(4) .  '_' . strtolower($image->getClientOriginalName());

        FileHelper::upload($imagePath, $image->getContent());

        return $imagePath;
    }
}
