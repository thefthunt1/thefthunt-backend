<?php

namespace App\Services;

use App\Helper\FileHelper;
use Illuminate\Support\Str;

class ImageService
{

    public function uploadAndGetImagePath($image, $basePath = ''): string
    {
        $imagePath = $basePath . Str::random(4) .  '_' . strtolower($image->getClientOriginalName());

        FileHelper::upload($imagePath, $image->getContent());

        return $imagePath;
    }
}
