<?php

namespace App\Services;

use App\Helper\FileHelper;
use App\Models\Item;
use App\Models\ItemImages;
use App\Repositories\ItemRepositoryInterface;
use Illuminate\Support\Str;

class ItemService
{

    public function __construct(
        private ItemRepositoryInterface $itemRepository
    )
    {
    }

    public function uploadAndSaveImages(Item $item, array $images = []): void
    {
        $itemImageData = [];
        foreach ($images as $image) {
            $basePath = "items/$item->id/images/";
            $imagePath = $basePath . Str::random(4) .  '_' . strtolower($image->getClientOriginalName());

            FileHelper::upload($imagePath, $image->getContent());

            $itemImageData[] = [
                'item_id' => $item->id,
                'image_path' => $imagePath,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        $this->itemRepository->insert(app(ItemImages::class)->getTable(), $itemImageData);
    }

}
