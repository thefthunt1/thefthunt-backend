<?php

namespace App\Services\Payments\Gateways;

use App\Models\User;
use App\Services\Payments\GatewayResponse;
use App\Services\Payments\PaymentGatewayContract;

use function config;

class StripeGateway implements PaymentGatewayContract
{

    public function initialize(User $user, array $data = []): GatewayResponse
    {
        $itemId = '&item_id=' . $data['item_id'];

        $checkout =  $user->checkoutCharge(1200, 'T-Shirt',
            sessionOptions: [
                'success_url' => config('payments.stripe.success_url') . $itemId,
                'cancel_url' => config('payments.stripe.success_url') . $itemId,
            ]
        );

        return new GatewayResponse(data: ['url' => $checkout->url]);
    }

    /**
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function response(User $user, array $data = []): GatewayResponse
    {
        $sessionId = $data['session_id'];
        $checkoutSession = $user->stripe()->checkout->sessions->retrieve($sessionId, ['expand' => ['line_items']]);

        if ($checkoutSession->payment_status == 'paid') {
            return new GatewayResponse();
        }

        return new GatewayResponse(message: 'Payment failed', success: false, code: 400);
    }
}
