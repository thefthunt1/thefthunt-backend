<?php

namespace App\Services\Payments;

use App\Models\User;

interface PaymentGatewayContract
{
    public function initialize(User $user, array $data = []): GatewayResponse;

    public function response(User $user, array $data = []): GatewayResponse;
}
