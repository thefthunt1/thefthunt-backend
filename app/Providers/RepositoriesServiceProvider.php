<?php

namespace App\Providers;

use App\Repositories\BaseRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CommentRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;
use App\Repositories\Implementation\BaseRepository;
use App\Repositories\Implementation\CategoryRepository;
use App\Repositories\Implementation\CommentRepository;
use App\Repositories\Implementation\CountryRepository;
use App\Repositories\Implementation\ItemRepository;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\ItemRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{

    /**
     * All the container bindings that should be registered.
     *
     * @var array
     */
    public array $bindings = [
        BaseRepositoryInterface::class => BaseRepository::class,
        UserRepositoryInterface::class => UserRepository::class,
        CountryRepositoryInterface::class => CountryRepository::class,
        CategoryRepositoryInterface::class => CategoryRepository::class,
        ItemRepositoryInterface::class => ItemRepository::class,
        CommentRepositoryInterface::class => CommentRepository::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
