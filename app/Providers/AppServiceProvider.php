<?php

namespace App\Providers;

use App\Services\Payments\Gateways\StripeGateway;
use App\Services\Payments\PaymentGatewayContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            PaymentGatewayContract::class,
            function ($app) {
                $gateway = request()->header('gateway');

                return match ($gateway) {
                    'stripe' => new StripeGateway(),
                    default => new StripeGateway(),
                };
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
