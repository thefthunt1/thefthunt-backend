<?php

namespace App\Repositories;

use App\Models\User;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function getAll();

    public function hasBookmarkedItem(User $user, int $itemId): bool;

    public function bookmarkItem(User $user, int $itemId): bool;
}
