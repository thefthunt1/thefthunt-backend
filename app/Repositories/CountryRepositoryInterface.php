<?php

namespace App\Repositories;

interface CountryRepositoryInterface extends BaseRepositoryInterface
{
    public function getAll();
}
