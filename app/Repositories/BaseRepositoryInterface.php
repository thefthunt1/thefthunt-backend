<?php

namespace App\Repositories;

use App\Models\User;

interface BaseRepositoryInterface
{
    public function index($model, array $with = []);

    public function store($model, array $data);

    public function update($model, array $data): int;

    public function findWithoutGlobalScopes($model, int $id, array $with = []);

    public function whereIn($model, array $values);

    public function find($model, int $id, array $with = []);

    public function findBySlug($model, string $slug);

    public function findByColumn($model, string $column, string $value, $first = true, array $with = []);

    public function findRandom($model, array $with = []);

    public function softDelete(string $table, int $id, User $user, array $additionalFields = []): int;

    public function delete(string $table, int $id): bool;

    public function insert(string $table, array $data): bool;

    public function insertE($model, array $data);
}
