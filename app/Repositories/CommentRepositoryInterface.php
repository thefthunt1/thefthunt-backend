<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CommentRepositoryInterface extends BaseRepositoryInterface
{

    public function getAll(): LengthAwarePaginator;

}
