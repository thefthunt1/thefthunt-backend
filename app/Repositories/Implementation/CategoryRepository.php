<?php

namespace App\Repositories\Implementation;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * Returns more data.
     *
     */
    public function getAll(): array|Collection
    {
        return QueryBuilder::for(Category::class)
            ->with(['children', 'children.children'])
            ->allowedFilters(['name'])
            ->withoutGlobalScopes()
            ->whereDoesntHave('parent')
            ->get();
    }
}
