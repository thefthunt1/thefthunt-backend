<?php

namespace App\Repositories\Implementation;

use App\Models\Item;
use App\QueryBuilder\Filters\Item\FilterSearchTerm;
use App\Repositories\ItemRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ItemRepository extends BaseRepository implements ItemRepositoryInterface
{
    /**
     * Returns more data.
     *
     */
    public function getAll(): LengthAwarePaginator
    {
        return QueryBuilder::for(Item::class)
            ->with(['user', 'type', 'category', 'images'])
            ->allowedFilters(
                [
                    'user_id',
                    'name',
                    'color',
                    'brand',
                    'status',
                    AllowedFilter::partial('user', 'user.name'),
                    AllowedFilter::exact('category', 'category.id'),
                    AllowedFilter::scope('created_between'),
                    AllowedFilter::custom('searchTerm', new FilterSearchTerm()),

                ])
            ->defaultSort('-id')
            ->allowedSorts(
                'id',
                'status',
                'price',
                'created_at',
                'updated_at',
            )
//            ->withoutGlobalScopes()
            ->paginate(config('admin.pagination.default'));
    }

    public function getRelatedItems(Item $item, int $number)
    {
        return Item::with('images', 'category')
            ->whereNot('id', $item->id)
            ->where('category_id', $item->category_id)
            ->take($number)->get();
    }
}
