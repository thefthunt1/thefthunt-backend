<?php

namespace App\Repositories\Implementation;

use App\Models\Country;
use App\QueryBuilder\Filters\Country\FilterSearchTerm;
use App\Repositories\CountryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CountryRepository extends BaseRepository implements CountryRepositoryInterface
{
    /**
     * Returns more data.
     *
     */
    public function getAll(): Collection|array
    {
        return QueryBuilder::for(Country::class)
            ->allowedFilters(
                [
                    AllowedFilter::exact('id'),
                    'name',
                    'abbreviation',
                    AllowedFilter::custom('searchTerm', new FilterSearchTerm()),
                ]
            )
            ->defaultSort('id')
            ->allowedSorts(
                'id',
                'name',
                'abbreviation',
            )
            ->withoutGlobalScopes()
//            ->take(config('admin.pagination.default'))
            ->get();
    }
}
