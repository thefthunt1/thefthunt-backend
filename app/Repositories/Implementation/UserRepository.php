<?php

namespace App\Repositories\Implementation;

use App\Models\Item;
use App\Models\User;
use App\QueryBuilder\Filters\User\FilterSearchTerm;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Returns more data.
     *
     */
    public function getAll(): LengthAwarePaginator
    {
        return QueryBuilder::for(User::class)
            ->with(['country'])
            ->allowedFilters(
                [
                    AllowedFilter::exact('id'),
                    'name',
                    'email',
                    'address',
                    'postcode',
                    'city',
                    AllowedFilter::partial('country', 'country.name'),
                    AllowedFilter::scope('created_between'),
                    AllowedFilter::custom('searchTerm', new FilterSearchTerm()),
                ]
            )
            ->defaultSort('-id')
            ->allowedSorts(
                'id',
                'name',
                'email',
                'created_at',
                'updated_at',
            )
//            ->withoutGlobalScopes()
            ->paginate(config('admin.pagination.default')); // Soft delete is a global scope
    }

    public function hasBookmarkedItem(User $user, int $itemId): bool
    {
        return $user->bookmarkList()->where(app(Item::class)->getTable() . '.id', $itemId)->exists();
    }

    public function bookmarkItem(User $user, int $itemId): bool
    {
        $query = $user->bookmarkList();
        $bookmarked = $this->hasBookmarkedItem($user, $itemId);
        if ($bookmarked) {
            $query->detach($itemId);
        }else{
            $query->attach($itemId);
        }

        return !$bookmarked;
    }
}
