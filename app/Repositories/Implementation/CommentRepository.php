<?php

namespace App\Repositories\Implementation;

use App\Models\Comment;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    /**
     * Returns more data.
     *
     */
    public function getAll(): LengthAwarePaginator
    {
        return QueryBuilder::for(Comment::class)
            ->allowedFilters(
                [
                    AllowedFilter::exact('user', 'user_id'),
                    AllowedFilter::exact('item', 'item_id')
                ]
            )
            ->defaultSort('-id')
            ->withoutGlobalScopes()
            ->paginate(config('api.pagination.comments.list'));
    }
}
