<?php

namespace App\Repositories\Implementation;

use App\Models\User;
use App\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Facades\DB;

class BaseRepository implements BaseRepositoryInterface
{

    public function index($model, array $with = [])
    {
        $query = $model::withoutGlobalScopes();
        if (!empty($with)) {
            $query->with($with);
        }

        return $query->paginate(config('admin-panel.pagination.default'));
    }

    public function store($model, array $data)
    {
        return $model::create($data);
    }

    public function update($model, array $data): int
    {
        return $model->update($data);
    }

    public function findWithoutGlobalScopes($model, int $id, array $with = [])
    {
        if (!empty($with)) {
            return $model::with($with)->withoutGlobalScopes()->find($id);
        }

        return $model::withoutGlobalScopes()->find($id);
    }

    public function whereIn($model, array $values)
    {
        return $model::whereIn('id', $values)->get();
    }

    public function find($model, int $id, array $with = [])
    {
        if (!empty($with)) {
            return $model::with($with)->find($id);
        }

        return $model::find($id);
    }

    public function findBySlug($model, string $slug)
    {
        return $model::where('slug', $slug)->first();
    }

    public function findByColumn($model, string $column, string $value, $first = true, array $with = [])
    {
        $query = $model::where($column, $value);
        if ($with) {
            $query->with($with);
        }

        return $first ? $query->first() : $query->get();
    }

    public function findRandom($model, array $with = [])
    {
        return $model::inRandomOrder()->first();
    }

    public function softDelete(string $table, int $id, User $user, array $additionalFields = []): int
    {
        return DB::table($table)->where('id', '=', $id)
            ->update(
                array_merge(
                    [
                        'deleted_by' => $user->id,
                        'deleted_at' => now(),
                        'updated_by' => $user->id,
                        'updated_at' => now()
                    ],
                    $additionalFields
                )
            );
    }

    public function delete(string $table, int $id): bool
    {
        return DB::table($table)->delete($id);
    }

    public function insert(string $table, array $data): bool
    {
        return DB::table($table)->insert($data);
    }

    public function insertE($model, array $data)
    {
        return $model::create($data);
    }
}
