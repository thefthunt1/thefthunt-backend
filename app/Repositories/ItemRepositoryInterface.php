<?php

namespace App\Repositories;

use App\Models\Item;

interface ItemRepositoryInterface extends BaseRepositoryInterface
{
    public function getAll();

    public function getRelatedItems(Item $item, int $number);
}
