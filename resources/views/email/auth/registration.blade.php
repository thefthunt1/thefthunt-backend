@component('mail::message')
    <h2>Hello {{$body['name']}},</h2>
    <p>Welcome to Thefthunt, you have succefully registered!</p>
    @component('mail::button', ['url' => $body['url']])
        {{ $body['actionText'] }}
    @endcomponent
    <p>Happy searching!</p>
    <p>Thanks,
        {{ config('app.name') }}
    </p>
@endcomponent
