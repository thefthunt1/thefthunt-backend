<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ItemFactory extends Factory
{
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => rand(1,34),
            'item_type_id' => rand(1,2),
            'category_id' => Category::whereNotNull('parent_id')->get()->random()->id,
            'name' => fake()->word(),
            'active' => true,
            'description' => fake()->sentences(5, true),
            'brand' => fake()->word(),
            'color' => fake()->colorName(),
            'price' => fake()->randomFloat(2, 1, 100),
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime
        ];
    }
}
