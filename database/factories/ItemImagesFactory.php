<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\ItemImages;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ItemImagesFactory extends Factory
{
    protected $model = ItemImages::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'item_id' => Item::all()->random()->id,
            'image_path' => $this->faker->imageUrl,
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }
}
