<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Item;
use App\Models\ItemImages;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = Item::factory(70)->create();

        foreach ($items as $item) {
            ItemImages::factory(3)->create(
                [
                    'item_id' => $item->id,
                ]
            );
        }

        Comment::factory(5)->create(
            [
                'item_id' => 28,
            ]
        );
    }
}
