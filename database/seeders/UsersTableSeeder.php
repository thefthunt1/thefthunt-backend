<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'Test User',
            'email' => 'admin@test.com',
            'profile_image' => 'users/no-image.jpg',
            'password' => Hash::make('admin12345'),
        ]);

        User::factory(33)->create();
    }
}
