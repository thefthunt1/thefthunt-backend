<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name' => 'Lost',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Stolen',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ];

        DB::table('item_types')->insert($types);
    }
}
