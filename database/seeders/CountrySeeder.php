<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Http::withoutVerifying()
            ->get("https://restcountries.com/v3.1/all")
            ->json();

        $chunks = collect($countries)->sortBy('name.common')->chunk(100);

        foreach ($chunks as $chunk) {
            foreach ($chunk as $countryData) {
                Country::query()
                    ->firstOrCreate(['name' => Arr::get($countryData, 'name.common')], [
                        'name' => Arr::get($countryData, 'name.common'),
                        'abbreviation' => Arr::get($countryData, 'cca2'),
                    ]);
            }
        }
    }
}
