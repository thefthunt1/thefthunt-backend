<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ItemController;
use App\Http\Controllers\Admin\SharedController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Shared\Auth\AuthController;
use App\Http\Controllers\Shared\CountryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| ADMIN API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register ADMIN API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login'])->name('admin.login');

Route::group([ 'middleware' => ['auth:sanctum']], function () {
    Route::get('me', [AuthController::class, 'me']);
    Route::post('logout', [AuthController::class, 'logout']);


    Route::get('items/types', [ItemController::class, 'getItemTypes']);

    Route::get('initial', [SharedController::class, 'getInitialData']);

    Route::group(['prefix' => 'items/images', 'controller' => ItemController::class], function () {
        Route::delete('{id}', 'destroyImage');
    });

//    Route::post('items/{id}', [ItemController::class, 'update']); // Backup if needed because of images
    /*
     *   Verb	    URI	                    Action	    Route Name
     *   GET	    /users	                index	    users.index
     *   GET	    /users/create	        create	    users.create
     *   POST	    /users	                store	    users.store
     *   GET	    /users/{photo}	        show	    users.show
     *   GET	    /users/{photo}/edit	    edit	    users.edit
     *   PUT/PATCH	/users/{photo}	        update	    users.update
     *   DELETE	    /users/{photo}	        destroy	    users.destroy
     */
    Route::resources([
        'users' => UserController::class,
        'categories' => CategoryController::class,
        'items' => ItemController::class
    ]);


    Route::get('countries', [CountryController::class, 'index']);
});
