<?php

use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\CommentController;
use App\Http\Controllers\API\HomepageController;
use App\Http\Controllers\API\ItemController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\PaymentController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\Shared\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth/{provider}', 'controller' => AuthController::class], function () {
    Route::get('', 'redirectToProvider');
    Route::post('callback', 'handleProviderCallback');
    Route::get('test', 'testProviderCallback');
});

//Route::group(['prefix' => 'callback', 'controller' => AuthController::class], function () {
//    Route::get('{provider}', 'handleProviderCallback');
//});

Route::post('login', [AuthController::class, 'login'])->name('api.login');
Route::post('register', [AuthController::class, 'register'])->name('api.register');

Route::group(['prefix' => 'items', 'controller' => ItemController::class], function () {
    Route::get('', 'index');
    Route::get('create', 'create');
    Route::get('{id}', 'show');
});

Route::group(['prefix' => 'categories', 'controller' => CategoryController::class], function () {
    Route::get('{id}', 'show');
});

Route::group(['prefix' => 'comments', 'controller' => CommentController::class], function () {
    Route::get('', 'index');
});

Route::group([ 'middleware' => ['auth:sanctum']], function () {
    Route::get('me', [AuthController::class, 'me']);
    Route::post('logout', [AuthController::class, 'logout']);

    Route::group(['prefix' => 'order', 'controller' => OrderController::class], function () {
        Route::post('', 'create')->name('order.store');
        Route::get('postback', 'postback')->name('order.postback');
    });

    Route::group(['prefix' => 'homepage', 'controller' => HomepageController::class], function () {
        Route::get('', 'index');
    });

    Route::group(['prefix' => 'bookmark', 'controller' => UserController::class], function () {
        Route::get('list', 'bookmarkList')->name('bookmarks.list');
        Route::post('', 'bookmarkItem');
    });

    Route::group(['prefix' => 'comments', 'controller' => CommentController::class], function () {
        Route::post('', 'store')->name('comments.store');
        Route::patch('{id}', 'update')->name('comments.update');
        Route::delete('{id}', 'destroy')->name('comments.destroy');
    });

});
