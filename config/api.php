<?php

return [
    'pagination' => [
        'default' => 7,
        'items' => [
            'list' => 32
        ],
        'comments' => [
            'list' => 10
        ]
    ],
    'users' => [
        'no-image' => 'users/no-image.jpg'
    ]
];
