<?php

return [
    'stripe' => [
        'success_url' => 'http://127.0.0.1:5173/payment/success?session_id={CHECKOUT_SESSION_ID}',
        'cancel_url' => 'http://127.0.0.1:8000/payment/canceled?session_id={CHECKOUT_SESSION_ID}'
    ]
];
