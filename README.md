<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## Requirements

- php 8 or above

## Installation

- composer install
- copy .env.example to .env (adjust db params)
- php artisan storage:link
- php artisan optimize:clear
- php artisan migrate --seed
- php artisan serve

## Test credentials

email: admin@test.com
pass: admin12345
